import java.util.Stack;

public class ValidParentheses {
//    Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

//    An input string is valid if:
//
//    Open brackets must be closed by the same type of brackets.
//    Open brackets must be closed in the correct order.
//    Every close bracket has a corresponding open bracket of the same type.
//
//
//    Example 1:
//
//    Input: s = "()"
//    Output: true
//    Example 2:
//
//    Input: s = "()[]{}"
//    Output: true
//    Example 3:
//
//    Input: s = "(]"
//    Output: false
//
//
//    Constraints:
//
//    1 <= s.length <= 104
//    s consists of parentheses only '()[]{}'.

    public static void main(String[] args) {
        var input = "){";
        var isValid = isValid(input);
        System.out.println(isValid);
    }

    public static boolean isValid(String s) {
        if (s.length() % 2 != 0)
            return false;

        Stack<Character> dataStack = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            var current = s.charAt(i);
            var isLeftSide = current == '{' || current == '[' || current == '(';
            if (isLeftSide)
                dataStack.push(current);
            if (!isLeftSide) {
                if (dataStack.empty())
                    return false;
                var popOne = dataStack.pop();
                switch (current) {
                    case '}':
                        if (popOne != '{')
                            return false;
                        break;
                    case ']':
                        if (popOne != '[')
                            return false;
                        break;
                    case ')':
                        if (popOne != '(')
                            return false;
                        break;
                    default:
                        return false;
                }

            }

        }

        if (!dataStack.empty())
            return false;

        return true;
    }
}
